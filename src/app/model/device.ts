export interface Device {
  id: number;
  name: string;
  memory: number;
  date: number;
  price: number;
  country: string;
  os: string;
}
/*

export type PartialDevice = Partial<Device>;
export type FormDevice = Pick<Device, 'name' | 'price'>

*/
