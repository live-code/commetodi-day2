import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/layout/theme.service';

@Component({
  selector: 'com-home',
  template: `
    <div>
      <button 
        class="btn btn-primary"
        [ngClass]="{'bg-warning': themeService.value === 'light'}"
        (click)="themeService.changeTheme('light')"
      >light</button>
      <button
        class="btn btn-primary"
        [ngClass]="{'bg-warning': themeService.value === 'dark'}"
        (click)="themeService.changeTheme('dark')"
      >dark</button>
    </div>
  `,
  styles: []
})
export class HomeComponent implements OnInit {

  constructor(public themeService: ThemeService) {
  }

  ngOnInit(): void {
  }

}
