import { Component, ViewChild } from '@angular/core';
import { Device } from '../../model/device';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'com-catalog',
  template: `

    <div class="container mt-3">
      <div class="alert alert-danger" *ngIf="error">Errore server</div>

      <!-- <div *ngIf="inputName.errors?.required && f.dirty">Il campo NAME è obbligatorio</div>
       <div *ngIf="inputName.errors?.minlength">Il campo è troppo corto. Mancano XX caratteri</div>
     -->
      <div *ngIf="inputName.errors?.minlength">
        Mancano {{inputName.errors?.minlength.requiredLength - inputName.errors?.minlength.actualLength }} caratteri
      </div>

      <form #f="ngForm" (submit)="save(f)">
        <div class="form-group">
          <input
            type="text"
            name="name"
            [ngModel]="active?.name"
            #inputName="ngModel"
            required
            minlength="3"
            class="form-control"
            [ngClass]="{
              'is-invalid': inputName.invalid && f.dirty,
              'is-valid': inputName.valid
            }"
            placeholder="Device Name"
          >
          <com-input-bar [inputRef]="inputName"></com-input-bar>
        </div>

        <input
          type="text"
          name="price"
          [ngModel]="active?.price"
          #inputPrice="ngModel"
          required
          class="form-control"
          placeholder="Price"
          [ngClass]="{
            'is-invalid': inputPrice.invalid && f.dirty,
            'is-valid': inputPrice.valid
          }"
        >
        <com-input-bar [inputRef]="inputPrice.errors"></com-input-bar>

        <select
          class="form-control" required
          [ngModel]="active?.os"
          name="os"
          #inputOs="ngModel"
          [ngClass]="{
            'is-invalid': inputOs.invalid && f.dirty,
            'is-valid': inputOs.valid
          }"
        >
          <option [ngValue]="null">Select OS</option>
          <option value="android">Android</option>
          <option value="ios">Apple</option>
        </select>


        <!-- <div class="btn-group">-->
        <button
          class="btn"
          [ngClass]="{
              'btn-success': f.valid,
              'btn-danger': f.invalid
            }"
          type="submit" [disabled]="f.invalid">
          {{active ? 'EDIT' : 'ADD'}}
        </button>
        <button type="button" class="btn btn-outline-primary" (click)="resetHandler(f)">RESET</button>
        <!--</div>-->
      </form>

      <hr>

      <li
        *ngFor="let device of devices"
        (click)="setActive(device)"
        class="list-group-item"
        [ngClass]="{
          'active': device.id === active?.id
        }"
      >
        <com-icon [os]="device.os"></com-icon>

        <!--<os-icon value="fa fa-android"></os-icon>-->

        {{device.name}}

        <span class="pull-right">
            <i class="fa fa-trash" (click)="deleteHandler(device.id, $event)"></i>
        </span>

        <span class="pull-right">
          <com-price [price]="device.price"></com-price>
        </span>
      </li>
    </div>
  `,
  styles: []
})
export class CatalogComponent {
  devices: Device[];
  active: Device;
  error: boolean;

  constructor(private http: HttpClient) {
    http.get<Device[]>('http://localhost:3000/devices')
      .subscribe((result) => {
        this.devices = result;
      });
  }


  deleteHandler(id: number, event: MouseEvent) {
    event.stopPropagation();
    this.http.delete(`http://localhost:3000/devices/${id}`)
      .subscribe(
        () => {
          const index = this.devices.findIndex(d => d.id === id);
          this.devices.splice(index, 1);
          this.error = false;
        },
        (err) => {
          this.error = true;
        }
      );
  }

  save(form: NgForm) {
    if (this.active) {
      this.edit(form);
    } else {
      this.add(form);
    }
  }

  edit(form: NgForm) {
    // const device: Device = form.value as Device;
    const device: Device = {
      ...form.value,
      price: +form.value.price
    }

    this.http.put<Device>(`http://localhost:3000/devices/${this.active.id}`, device)
      .subscribe(res => {
        const index = this.devices.findIndex(d => d.id === this.active.id);
        this.devices[index] = res;
      });
  }
  add(form: NgForm) {
    const device: Device = form.value as Device;
    this.http.post<Device>(`http://localhost:3000/devices/`, device)
      .subscribe(res => {
        this.devices.push(res);
        form.reset();
      });
  }

  setActive(device: Device) {
    console.log('set active')
    this.active = device;
  }

  resetHandler(f: NgForm) {
    f.reset();
    this.active = null;
  }
}
