import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HelloComponent } from './components/hello.component';
import { IconComponent } from './components/icon.component';
import { PriceComponent } from './components/price.component';
import { InputBarComponent } from './components/input-bar.component';
import { HomeComponent } from './features/home/home.component';
import { CatalogComponent } from './features/catalog/catalog.component';
import { LoginComponent } from './features/login/login.component';
import { UikitComponent } from './features/uikit/uikit.component';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './core/components/navbar.component';
import { ThemeService } from './core/layout/theme.service';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    HomeComponent,
    LoginComponent,
    UikitComponent,
    NavbarComponent,
    // feature catalog
    CatalogComponent,
    IconComponent,
    PriceComponent,
    InputBarComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'catalog', component: CatalogComponent },
      { path: 'home', component: HomeComponent },
      { path: 'login', component: LoginComponent },
      { path: 'uikit', component: UikitComponent },
      { path: '', component: HomeComponent },
      { path: '**', redirectTo: 'home' },
    ])
  ],
  providers: [
    ThemeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(theme: ThemeService) {
    theme.init();
  }
}

