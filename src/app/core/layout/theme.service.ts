import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay } from 'rxjs/operators';

interface ThemeProps {
  value: string;
  fontSize: number;
}

@Injectable()
export class ThemeService {
  value = 'dark';

  constructor(private http: HttpClient) {
    console.log('init')
  }

  init() {
    this.http.get<ThemeProps>('http://localhost:3000/theme')
      .subscribe((res) => {
        this.value = res.value;
      });
  }

  changeTheme( value: string ) {
    this.http.patch('http://localhost:3000/theme', { value })
      .subscribe(() => {
        this.value = value;
      });

  }
}
