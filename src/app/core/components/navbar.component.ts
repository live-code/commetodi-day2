import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../layout/theme.service';

@Component({
  selector: 'com-navbar',
  template: `
    <nav 
      class="navbar navbar-expand"
      [ngClass]="{
        'navbar-light bg-light': themeService.value === 'light',
        'navbar-dark bg-dark': themeService.value === 'dark'
      }"
    >
      <a class="navbar-brand">Navbar {{themeService.value}}</a>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-item nav-link" routerLink="home" routerLinkActive="active">Home</a>
          <a class="nav-item nav-link" routerLink="catalog" routerLinkActive="active">Catalog</a>
          <a class="nav-item nav-link" routerLink="login" routerLinkActive="active">login</a>
          <a class="nav-item nav-link" routerLink="uikit" routerLinkActive="active">uikit</a>
        </div>
      </div>
    </nav>
  `,
  styles: []
})
export class NavbarComponent  {

  constructor(public themeService: ThemeService) {
  }
}

