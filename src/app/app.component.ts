import { Component } from '@angular/core';

@Component({
  selector: 'com-root',
  template: `
    <com-navbar></com-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {

}
