import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'com-price',
  template: `
    <span 
      [style.color]="getPriceClass()">
      € {{price}}
    </span>

  `,
  styles: []
})
export class PriceComponent {
  @Input() price: number;

  getPriceClass() {
    return this.price >= 1000 ? 'red' : null;
  }
}
