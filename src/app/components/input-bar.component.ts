import { Component, Input, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'com-input-bar',
  template: `
    <div class="bar" [style.width.%]="getBarPercentage()"></div>
  `,
  styles: [`
    .bar { 
      background-color: grey; 
      width: 100%; 
      height: 10px; 
      border-radius: 5px;
      transition: all 1s cubic-bezier(0.68, -0.55, 0.265, 1.55);
    }
  `]
})
export class InputBarComponent {
  @Input() inputRef: NgModel;

  getBarPercentage() {
    if (
      this.inputRef &&
      this.inputRef.errors &&
      this.inputRef.errors.minlength
    ) {
      const perc = (this.inputRef.errors.minlength.actualLength / this.inputRef.errors.minlength.requiredLength) * 100;
      return perc;
    }
    return 0;
  }
}
