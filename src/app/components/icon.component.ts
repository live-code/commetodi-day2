import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'com-icon',
  template: `
    <i
      class="fa"
      [ngClass]="{
        'fa-android': os === 'android',
        'fa-apple': os === 'ios'
      }"
    ></i>
  `,
  styles: []
})
export class IconComponent {
  @Input() os: string;
}
