import { Component, Input } from '@angular/core';

@Component({
  selector: 'com-hello',
  template: `
   <div [style.color]="color"> Hello {{name}}</div>
  `
})
export class HelloComponent {
  @Input() name = 'Guest';
  @Input() color = 'red';
}
